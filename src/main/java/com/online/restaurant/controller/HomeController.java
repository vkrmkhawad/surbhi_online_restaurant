package com.online.restaurant.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.online.restaurant.service.UsersService;

@RestController
public class HomeController {
	
    @GetMapping("/")
    public String home() {
        return ("<h1>Welcome vist http://localhost:8080/swagger-ui.html for operations</h1>");
    }
}
