package com.online.restaurant.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.online.restaurant.config.AspectConfig;
import com.online.restaurant.entity.FinancialReport;
import com.online.restaurant.entity.ItemDetails;
import com.online.restaurant.model.SelectItem;
import com.online.restaurant.service.RestaurantItemService;


@RestController
@RequestMapping("/RestaurantItems")
public class RestaurantItemController {
	
	@Autowired
	RestaurantItemService restaurantItemService;
	
	@GetMapping("/viewItems")
	public List<ItemDetails> viewItems(){
		return restaurantItemService.viewItemDetails();
	}

	@PostMapping("/selectItemsbyId")
	public String selectItemDetailsbyId(SelectItem selectItem){
		return restaurantItemService.selectItemDetailsbyId(selectItem);
	}
	
	@PostMapping("/selectItemsbyIds")
	public String selectItemDetailsByIds(@RequestBody List<SelectItem> selectItem){
		return restaurantItemService.selectItemDetailsByIds(selectItem);
	}
	
	@GetMapping("/viewFinalBill")
	public List<FinancialReport> getFinalBill(){
		return restaurantItemService.viewFinalBill();
	}

	@GetMapping("/viewDailyBillings")
	public List<FinancialReport> getDailyReport(){
		return restaurantItemService.viewDailyReport();
	}
	
	@GetMapping("/viewMonthlyBillings")
	public List<FinancialReport> getMonthlyReport(){
		return restaurantItemService.totalMonthlySale();
	}
}
