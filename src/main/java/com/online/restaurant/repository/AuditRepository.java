package com.online.restaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.online.restaurant.entity.AuditLog;

@Repository
public interface AuditRepository extends JpaRepository<AuditLog, Integer>{

}
