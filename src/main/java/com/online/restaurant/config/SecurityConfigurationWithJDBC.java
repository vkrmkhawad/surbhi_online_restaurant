package com.online.restaurant.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfigurationWithJDBC extends WebSecurityConfigurerAdapter{

    @Autowired
    UserDetailsService userDetailsService;
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(getPasswordEncoder());
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers("/RestaurantItems/viewDailyBillings","/RestaurantItems/viewMonthlyBillings","/registerUser/registrationByAdmin","/registerUser/viewAllUsers","/registerUser/deleteUser","/registerUser/updateUserPassword","/registerUser/updateUserRole").hasRole("ADMIN")
                .antMatchers("/RestaurantItems/viewItems","/RestaurantItems/selectItemsbyId","/RestaurantItems/selectItemsbyIds","/RestaurantItems/viewFinalBill").hasAnyRole("ADMIN", "USER")
                .antMatchers("/registerUser/registrationByUser").permitAll()
                .and().formLogin();
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }
}
