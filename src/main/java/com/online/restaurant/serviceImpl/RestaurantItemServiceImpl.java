package com.online.restaurant.serviceImpl;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.online.restaurant.entity.FinancialReport;
import com.online.restaurant.entity.ItemDetails;
import com.online.restaurant.model.SelectItem;
import com.online.restaurant.repository.FinancialReportRepository;
import com.online.restaurant.repository.ItemDetailsRepository;
import com.online.restaurant.service.RestaurantItemService;

@Service
public class RestaurantItemServiceImpl implements RestaurantItemService {

	@Autowired
	FinancialReportRepository financialReportRepository;

	@Autowired
	ItemDetailsRepository itemDetailsRepository;

	@Autowired
	EntityManager entityManager;

	@Override
	public List<ItemDetails> viewItemDetails() {
		return itemDetailsRepository.findAll();
	}

	@Override
	public String selectItemDetailsbyId(SelectItem selectItem) {
		TypedQuery<Integer> query1 = entityManager
				.createQuery("Select max(l.billId) from FinancialReport l", Integer.class);
		Integer i;
		if(query1.getResultList().isEmpty()) {
			i=0;
		}
		else {
			if(query1.getResultList().get(0) !=null)
				i=query1.getResultList().get(0);
			else
				i=0;
		}
		FinancialReport financialReport = new FinancialReport();
		Optional<ItemDetails> itemDetails;
		itemDetails = itemDetailsRepository.findById(selectItem.getItemId());
		financialReport.setItemName(itemDetails.get().getItemName());
		financialReport.setUsername(null);
		financialReport.setBillId(i+1);
		financialReport.setPurchaseDate(new java.util.Date(System.currentTimeMillis()));
		financialReport.setQty(selectItem.getQty());
		financialReport.setPrice(itemDetails.get().getPrice() * selectItem.getQty());
		financialReportRepository.saveAndFlush(financialReport);

		return "Items selected";
	}

	@Override
	public String selectItemDetailsByIds(List<SelectItem> selectItem) {
		TypedQuery<Integer> query1 = entityManager
				.createQuery("Select max(l.billId) from FinancialReport l", Integer.class);
		Integer cnt;
		if(query1.getResultList().isEmpty()) {
			cnt=0;
		}
		else {
			if(query1.getResultList().get(0) !=null)
				cnt=query1.getResultList().get(0);
			else
				cnt=0;
		}
		cnt=cnt+1;
		Optional<ItemDetails> itemDetails;
		for (int i = 0; i < selectItem.size(); i++) {
			FinancialReport financialReport = new FinancialReport();
			itemDetails = itemDetailsRepository.findById(selectItem.get(i).getItemId());
			financialReport.setItemName(itemDetails.get().getItemName());
			financialReport.setUsername(null);
			financialReport.setBillId(cnt);
			financialReport.setPurchaseDate(new java.sql.Date(System.currentTimeMillis()));
			financialReport.setQty(selectItem.get(i).getQty());
			financialReport.setPrice(itemDetails.get().getPrice() * selectItem.get(i).getQty());
			financialReportRepository.saveAndFlush(financialReport);
		}
		return "Items selected";
	}

	@Override
	public List<FinancialReport> viewFinalBill() {

		FinancialReport listAll = financialReportRepository.findAll(Sort.by(Direction.DESC, "billId")).get(0);
		int maxId = listAll.getBillId();
		FinancialReport finalBill = new FinancialReport();
		finalBill.setBillId(maxId);
		ExampleMatcher exampleMatcher = ExampleMatcher.matching()
				.withMatcher("finalcialReportToday", ExampleMatcher.GenericPropertyMatchers.exact())
				.withIgnorePaths("id","username", "itemName", "qty", "price", "purchaseDate");
		Example<FinancialReport> example = Example.of(finalBill, exampleMatcher);
		return financialReportRepository.findAll(example);
	}

	@Override
	public List<FinancialReport> viewDailyReport() {
		FinancialReport finalcialReportToday = new FinancialReport();
		finalcialReportToday.setPurchaseDate(new java.sql.Date(System.currentTimeMillis()));
		ExampleMatcher exampleMatcher = ExampleMatcher.matching()
				.withMatcher("finalcialReportToday", ExampleMatcher.GenericPropertyMatchers.exact())
				.withIgnorePaths("id", "username", "itemName", "qty", "price","billId");
		Example<FinancialReport> example = Example.of(finalcialReportToday, exampleMatcher);
		System.out.println("Example count :" + financialReportRepository.findAll(example).size());
		return financialReportRepository.findAll(example);
	}

	@Override
	public List<FinancialReport> totalMonthlySale() {
		Date date = new Date();
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int year = localDate.getYear();
		int month = localDate.getMonthValue();
		TypedQuery<FinancialReport> query1 = entityManager
				.createQuery("Select l from FinancialReport l where extract(month from l.purchaseDate)=" + month
						+ " and extract(year from l.purchaseDate)=" + year, FinancialReport.class);
		return query1.getResultList();

	}

}
