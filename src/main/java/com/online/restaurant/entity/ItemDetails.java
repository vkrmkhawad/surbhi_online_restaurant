package com.online.restaurant.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table
public class ItemDetails {

	@Id
	private Integer id;
	
	@Column(name = "ITEM_NAME")
	private String itemName;
	
	@Column(name = "PRICE")
	private int price;
}
