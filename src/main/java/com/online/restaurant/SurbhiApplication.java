package com.online.restaurant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.online.restaurant.repository.UsersRepository;


@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = UsersRepository.class)
public class SurbhiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SurbhiApplication.class, args);
	}

}
